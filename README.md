# Gitee 前端技术博客

记录一些技术问题和解决方法。

## 用法

```bash
# 安装依赖
npm install

# 新建一篇文章
hexo new post 文章标题

# 在本地运行服务，查看网站
hexo server

# 部署网站到 Gitee Pages
hexo deploy
```
