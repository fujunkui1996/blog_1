---
title: Pull Request 的多行评论功能开发日志
date: 2021-04-21 18:46:33
tags:
  - react
  - react-diff-view
  - 工作记录
---

{% asset_img gitee-diff-view-multiline-comment.gif %}

Pull Request 的多行评论功能允许用户选择指定范围内的代码行进行评论，使得评论上下文更准确，这个功能在之前的赶进度期间由于开发成本较高而被暂时搁置，一直推迟到最近才开始开发。

实现多行评论功能所需要解决的问题主要是鼠标交互、多行选中效果以及如何修改旧代码，本文章将介绍这些问题细节和解决方法。

## 高亮选中行

在按住 `+` 按钮拖动的时候和 hover 多行评论的时候需要高亮选中行，react-diff-view 的 Diff 组件本身有支持该功能，向 Diff 组件传入 selectedChanges 参数即可实现，至于如何生成 selectedChanges，解决方法如下：

- 监听文件内的 mousedown、mouseup、mouseenter 事件。在 mousedown 时记录起始行；在 mouseenter 时记录结束行，然后在 diff 数据中查找匹配的行数据，将其转换成 changeKey 数组存入 selectedChanges；在 mouseup 时结束选中操作，触发显示评论框。
- 监听评论的 mouseenter 和 mouseleave 事件。在 mouseenter 时根据评论数据中的起始和结束行号，从 diff 数据中筛选出匹配的行数据，将其转换成 changeKey 数组存入 selectedChanges；在 mouseleave 时，清空 selectedChanges。

在这两个解决方案中稍微复杂点的功能是行数据的筛选，筛选条件有两种：起始行数据 + 行数、起始行的新旧行号 + 结束行的新旧行号，核心操作都是遍历每个 hunk 中的每个 change，判断 change 中的 oldLineNumber 和 newLineNumber 这两个属性是否符合条件。

选中效果是有了，但 Diff 组件只是简单的给选中行加上 `diff-code-selected` 类，无法区分起始行和结束行，要给起始行和结束行加上边界框样式的话，还需要手动编码实现。现在采用的方法是用 `useEffect()` + 原生 DOM 操作，在每次渲染后获取所有代码行的 DOM 对象，给起始行和结束行加上对应的 className。

## 重构

在旧代码中，文件差异视图组件包含了评论、按需加载、展开更多等功能的代码，这杂乱的代码增加了新功能的开发成本，光是理清评论功能和 Diff 组件交互代码都得花点时间去上下滚动浏览代码。为解决这问题，提升多行评论功能的开发体验，有必要对旧代码进行重构，重构方案如下：

- 将评论功能的状态管理和组件相关代码都移动到 `useCommentWidgets.jsx` 中，主组件中调用 `useCommentWidgets()` 并传入响应参数即可启动代码评论。
- 将展开更多功能的代码移动到 `ExpandableHunks.jsx`，在主组件中渲染 `<ExpandableHunks>` 组件即可启用该功能。

重构后，源文件内的代码量减少了一半，多行评论功能也基于这种思路将代码拆分到了两个文件：

- useMultilineSelectState.js: 多行选择功能的状态管理，包含 selectedChanges 状态和用于标记选中行边界的 Effect。
- useMultilineSelectHandler.js: 多行选择功能的处理器，包含鼠标交互和筛选选中行的实现。

采用这种思路的好处是，每块功能的代码都放在独立的文件中，在修改和添加新功能的时候可以更专注于某块功能的代码，不用再受到其它功能代码的影响而分散注意力。
