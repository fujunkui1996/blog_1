---
title: 企业版多语言支持方案改进
date: 2021-03-16 16:28:29
tags:
    - react
    - react-i18next
    - i18n-ally
---

{% asset_img vscode-i18n-ally.png %}

## 需求

现有的多语言支持方案存在以下问题：

- 翻译配置中的 key 采用中文原文命名，与旧企业版和社区版的 key 命名风格不同，不符合团队成员的习惯
- 不支持复数形式的翻译，例如：`1 error, 2 errors`
- 不支持对文本 + 组件混合内容的翻译，例如：`There are <strong>123<strong> tasks`
- 代码健壮性未知，可能会增加维护成本
- 缺少文档，不易上手

## 解决方案

新的方案：

- 采用 react-i18next 实现多语言支持
- 采用 i18next-http-backend 实现翻译文件的加载
- 采用 i18next-browser-languageetector 侦测合适的语言
- 在 vscode 中安装 i18n-ally 插件，方便查看和编辑翻译文本
- 编写 build-1i8n.js 脚本合并多个翻译配置文件，以便按功能模块划分配置文件分配给各个团队成员进行翻译，避免产生过多的冲突

### 约定

语言代号：

- en：英文
- zh-CN: 简体中文
- zh-TW: 繁体中文

翻译配置源文件存放位置：`config/locales`。

一级目录以语言代号命名，目录内的配置文件采用 json 格式，文件名与模块对应，例如：`issues.json`，它对应任务模块中用到的翻译配置。

翻译配置文件内的 key 命名采用小写+下划线风格，例如：`issues.filters.created_by_me`。

按模块拆分翻译文件只是为了方便维护，让前端直接加载这些文件的话只会增加网络请求数和等待时间，因此需要跑以下命令将它们合并成一个文件：

```bash
npm run build-i18n
```

为了有效的利用浏览器的缓存策略，前端在请求翻译文件时采用强缓存策略，并且请求参数中会带上版本号，该版本号来自 `config/locales` 文件夹的 commit id，由 DefinePlugin 插件在编译时将它作为 `APP_I18N_VERSION` 常量注入到源码中。

### 参考资料

- 文档：https://react.i18next.com/getting-started
- i18n 资源加载后端层：https://github.com/i18next/i18next-http-backend
- 语言侦测插件：https://github.com/i18next/i18next-browser-languageDetector
- i18n 初始化示例：https://github.com/i18next/react-i18next/blob/master/example/react/src/i18n.js
- 翻译配置文件示例：https://github.com/i18next/react-i18next/blob/master/example/react/public/locales/en/translation.json
- 用法示例：https://github.com/i18next/react-i18next/blob/master/example/react/src/App.js
