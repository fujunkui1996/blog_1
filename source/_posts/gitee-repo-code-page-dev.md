---
title: 仓库代码页开发总结
date: 2020-10-15 14:23:50
tags: react
---
## 需求

- 在导航栏中点击“新建文件夹”和“重命名文件”后，文件列表中会显示相应的表单
- 在导航栏中点击查找按钮并输入框关键词后，文件列表应该切换为搜索结果列表，按上下方向键切换上下文件，esc 键退出查找模式，enter 键进入文件详情页

## 组件化

以下是大致的组件结构：

- **ProjectCode:** 主页
  - **ProjectHeader:** 头部
    - **ProjectSocialButtons:** star、watch、fork 社交按钮组
  - **ProjectDescription:** 描述
  - **ProjectSummary:** 概要统计信息
  - **ProjectNavbar:** 导航栏
    - **RefDropdown:** 分支下拉框
    - **ProjectFinderInput:** 查找器的输入框
    - **ProjectBreadcrumb:** 面包屑
    - **ProjectCloneWayPopup:** 克隆/下载
  - **Switch:** react-router 的 Switch 组件
    - **Route -> ProjectTree:** 文件列表
      - **ProjectFinderFiles:** 查找器的文件列表
      - **ProjectRecentCommit:** 最近提交信息
      - **ProjectTreeItemFolderNewForm:** 文件夹新建表单
      - **ProjectTreeItem:** 文件列表项
        - **ProjectTreeItemRenameForm:** 文件重命名表单
      - **ProjectReadmeView:** 自述文件预览
    - **Route -> ProjectBlob:** 文件详情
- **ProjectComments:** 评论列表

## 数据模型

分析需求后可知新建文件夹、重命名文件和查找文件的相关状态在多个组件中共享，因此可以将数据源划分为两个：ProjectTreeOperation 和 ProjectFinder。

ProjectTreeOperation 保存文件列表视图的操作状态：

```js
{
    findFiles: false,
    newFilder: false,
    renameTarget: null
}
```

ProjectFinder 保存文件查找状态：

```js
{
  search: '',
  files: [],
  selectedFileIndex: 0
}
```

由于文件查找视图的文件上下切换选中功能需要多写几行代码判断下标的有效性，所以采用 useReducer() 将这些逻辑写进 reducer() 函数中，最终创建的 dispatch 方法支持的操作有三个：setFiles、prevFile、nextFile。

## 组件通信

对于新建文件夹和重命名文件操作，使用 ProjectTreeOperationStateContext 和 ProjectTreeOperationSetterContext 实现导航栏组件和文件列表组件的通信。对于文件查找操作，使用 ProjectFinderStateContext 和 ProjectFinderDispatchContext 实现输入框和文件列表组件的通信。
