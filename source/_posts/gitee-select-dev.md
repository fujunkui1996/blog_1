---
title: Select 组件的更换方案
date: 2021-06-17 13:55:58
tags:
---
## 需求

原 Select 组件存在的主要问题是代码理解成本较高，组件内的各种功能的代码都混合在一起，如果要修改某个功能还得找出相关的代码块，然后理清与其它代码块的依赖关系，再找出相关的状态和副作用，最后理解实现原理和工作流程。

这个问题会引起其它问题：

- 修改和及添加新功能的难度较大。
- bug 的修复难度较大，修完一个 bug 可能会引出新 bug。

因此，我们需要一个新的 Select 组件来解决这些问题。

## 解决方案

基于 rc-select 组件定制，理由如下：

- **用开源组件可减少组件的学习成本和维护成本。** ant-design 的 select 组件是基于 rc-select 实现的，ant-design 作为一个流行的企业级设计语言，已经经过众多项目的考验，它的 select 组件应该能应对大部分使用场景，而且经过这么久的迭代，出 bug 的几率应该很小。
- **易于定制 CSS 样式。** rc-select 只提供最基本的 css 样式，支持自定义 class 前缀，我们可以很容易的根据项目的视觉规范来定制样式。
- **支持虚拟列表。** 减少渲染长列表时的性能开销。

## 新功能

为了满足业务需求，我们添加了以下属性：

- **noBorder**: 是否使用无边框样式
- **autoWidth**: 是否自适应内容宽度
- **autoLoadSelectedOption**: 是否加载已选中项
- **remote**: 是否启用滚动加载远程选项列表
- **remoteSearch**: 是否使用远程搜索功能
- **onFetchMore**: 加载更多选项时的回调函数
- **optionRender**: 选项的渲染函数
- **searchInMenu**: 是否在下拉菜单中显示搜索框
- **extraOptions**: 额外的选项
- **options[].SingleSelectionOption**: 是否为单选选项

## 遇到的问题

- 性能问题。打开/关闭下拉菜单时都会触发回流，由于 CSS 代码量很大、CSS 规则复杂，导致回流耗时很长。
- 使用动画会导致拉菜单无法向上展开。
- 不能自定义触发器。选择框和下拉菜单是绑定在一起的，无法自定义选择框，主要应用场景有：

  - 将选择框内容自定义成 `文本：[选项1][选项2]` 这种格式。
  - 点击自定义组件时展开下拉菜单。

  这场景看上去更适合用 Dropdown 组件实现，但如果用 Dropdown 的话，就要重新实现一遍已经给 Select 组件中做过的样式和功能定制，成本有点高。

## 实现细节

在前几个版本中，虽然各个 prop 的功能实现代码已经被划分成若干个 Hook 函数，但存在以下问题：

- 多个 Hook 函数依赖同一个 prop。
- 一个 Hook 函数依赖另一个 Hook 函数创建的状态。
- `options` 的处理流程不清晰。`searchInMenu`、`extraOptions`、`optionRender` 都依赖 `options`，且都有各自的逻辑处理 `options`，当启用 `remote` 时，还得让它们能够处理由 `remote` 的函数管理的 `options`。

这些问题本质上是代码耦合问题，要解决它首先得理清这些代码关系，然后进行拆分和简化，大致的优化点如下：

- 这些新增的 prop 都是基于 Select 组件原有的 props 实现的，那么 prop 的 Hook 函数的工作模式可以改为从参数接收 props，返回编辑后的 props。
- `remote` 的 Hook 函数只需要管理 `options` 状态，然后将 `options`、`onSearch` 等属性插入到返回的 props 中。
- `searchInMenu` 在下拉菜单中添加了自定义的搜索框以代替 rc-select 原有的搜索框，将 onSearch 插入到 props 中实现了对搜索功能的代理。这种做法恰好让 `remote` 的代码也能响应到搜索变化，从而重新加载远程选项。
- `optionRender` 只管在 `optionRender` 函数有效时转换 `options` 成 `children`。

优化后的代码如下：

```js
function BaseSelect(props) {
  let selectProps = useSearchInMenu(props);
  selectProps = useExtraOptions(selectProps);
  selectProps = useSingleSelectionOptions(selectProps);
  selectProps = useOptionRender(selectProps);
  return createElement(RcSelect, selectProps);
}

function RemoteSelect(props) {
  const selectProps = useRemote(props);
  return createElement(BaseSelect, selectProps);
}
```

从这段代码中我们可以看出以下特点：

- hook 函数命名与 prop 名相同，便于查找相关代码。
- 所有 hook 函数都只需要修改 props，无复杂的返回值和参数依赖。

## 效果演示

http://gitee-frontend.gitee.io/gitee-ent-react/gitee-select-react/
