---
title: 评论组件的设计
date: 2020-10-13 15:01:47
tags: react
---
## 需求

主要需求如下：

- 添加、删除、编辑、回复
- 点击“回复”时，将当前评论内容转换成引用块（`<blockquote>`）并填充到对应的评论框中
- 在 Pull Request 详情页使用评论组件时，如果评论类型是代码评论，则显示其引用的代码片段和“查看详情”链接，点击链接后会定位到对应文件中的代码行
- 多个评论列表组件能够共用同一数据源

## 组件化

- CommentList：评论列表
- CommentItem：评论列表项
- CommentForm: 评论框表单

## 数据模型

状态：

- **list:**  评论列表
- **total:** 总评论数
- **loaded:** 是否已加载评论列表
- **forms:** 各个评论框表单的状态

操作：

- **load:** 加载评论数据
- **reset:** 重置评论数据为空
- **add:** 添加一条评论
- **reply:** 回复评论，将预置的回复内容填充到对应的评论框并激活它
- **cancel:** 取消回复，让对应的评论框最小化

伪代码如下：

```js
const state = {
  list: [],
  total: 0,
  loaded: false,
  forms: {}
}

const actions = {
  load() {},
  reset() {},
  add() {},
  reply() {},
  cancel() {},
  update() {},
  remove() {}
}
```

## 组件通信

使用 useReducer() 创建 state 和 dispatch 方法，然后分别用 CommentStateContext 和 CommentDispatchContext 传递给各个组件。

评论的回复功能使用 `state.forms[formKey]` 与对应的 CommentForm 组件通信，而“查看详情”链接的跳转功能则基于 location.hash 和 id 实现，在点击链接时调用传入的 `onOpenDetail()` 回调函数将 query 和 hash 更改为 `?tab=files#diff_note_123` 以通知 Pull Request 页面组件切换到与 files 对应的文件改动视图，之后该视图根据 hash 附带的评论 id 将浏览区域滚动到对应 id 的 DOM 元素上，至此就实现了代码评论的定位功能。

剩下的功能都只涉及 React 基本用法，不再做详细说明。
