---
title: 树形评论组件设计
date: 2020-10-14 14:10:40
tags: react
---

## 数据模型

后端接口返回的是树形结构的评论数据，考虑到现有评论组件的数据结构兼容性和增删改操作的复杂度，需要将数据扁平化成列表结构，如下图所示：

{% asset_img 'tree-comment-flat-1.png' '树形结构扁平化' %}

这么一看的话，似乎扁平化不够彻底，每个评论都还保有树形结构，可通过 children 数组递归遍历子评论树，虽说数组中存储的值本质上都是评论对象引用，但为了保持结构的简单性以及数据源的单一性，避免在删除和更新评论时还要纠结评论数组和评论的 children 数组要怎么改的问题，还需要做些调整，将每个评论的 children 数组改成用 id 引用评论，调整效果如下图所示：

{% asset_img 'tree-comment-flat-2.png' '树形结构扁平化' %}

考虑到所有结点评论组件都要从评论列表中筛选出自己的子评论，为了减少评论列表数据的遍历次数，需要在根组件中建立一个以评论 id 为索引键的映射表，供所有结点的评论组件使用。

## 组件化

树形结构的组件是由结点组件组成的，只需要实现一个结点组件然后递归遍历创建它们即可，大致结构如下：

- CommentTreeNode
  - CommentItem
    - CommentForm
    - CommentTreeNode
    - CommentTreeNode
    - CommentTreeNode
    - ...

## 组件通信

和评论列表组件一样，在父组件中使用 Context 将评论数据的 state 和 dispatch 方法传递给各级 CommentTreeNode 组件使用。
