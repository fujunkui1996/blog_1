---
title: 在开发环境中使用 vite 代替 webpack
date: 2021-10-15 16:39:53
tags:
---

## 需求

Gitee 企业版前端项目是采用 webpack 构建的，它存在以下问题：

- **构建速度慢：** 全量构建耗时很长，虽然可以通过配置缓存来提升构建速度，但这缓存会影响到 npm link 的私有包的调试，导致构建产出的代码一直用上个版本的包，必须禁掉缓存才会生效。
- **内存占用过大：** 构建时的内存占用会增长到 1 GB 以上，webpack-dev-server 运行时间长了后能占用 2 GB 以上的内存。
- **CPU 占用过高：** 构建时的 CPU 几乎满载，在 Ubuntu 系统中甚至能让系统 UI 出现卡顿问题。
- **热更新机制存在问题：** 更新速度慢，修改代码后是直接刷新页面而不是仅更新相关组件，影响开发效率，而且还存在内存占用过高的问题，浏览器标签页的内存占用会随着热更新次数的增加而增加，占用 1GB 以上的内存很常见。

## 分析

通过简单分析这些问题，我们可以很容易得出其主要原因在于模块的加载和转译，对于开发人员而言，在开发环境中调试用的浏览器都很新，无需考虑 JavaScript 新标准的兼容问题，那么我们可以将模块转译和 sourcemap 生成等流程的耗时作为主要指标，调研其它速度比 webpack 快的工具。

## 调研

首先排除 snowpack、parcel 和 rollup，它们在性能方面的提升有限且有一定的学习成本，剩下的就是
esbuild 和基于 esbuild 的 vite。esbuild 的文档中给出的性能对比图如下：

{% asset_img 'esbuild-performance.png' '性能对比' %}

可以看出 esbuild 的性能已经满足需求，但考虑到文档、配置复杂度、dev-server、sass-loader 等的问题，选择 vite 是最为合适的。

## 实施

此次的 vite 引入工作内容主要包括：

- 安装 vite 及相关依赖。
- 添加 vite.config.js 文件。
- 拆分 webpack 相关配置代码，以在 vite 配置中复用。
- 弃用 vite 不支持的装饰器语法，改成普通的函数调用。
- 升级 mobx，更新 mobx 用法，解决在 vite 构建模式下的 observer 组件不会响应 observe 状态更新的问题。
- 解决 vite:css 的警告。

由于引入 vite 主要用于解决开发环境下的性能问题，生产环境仍然用 webpack 构建，因此测试工作量较小。

## 问题记录

**node_modules 中的部分模块的 js 文件包含 JSX 语法**

添加插件，将目标模块的 js 文件的 loader 改成 jsx，参考：

https://github.com/vitejs/vite/discussions/3448#discussioncomment-749919


**Identifier 'React' has already been declared**

添加 esbuild 配置，给每个 jsx 文件注入 react 的引入代码，参考：

https://github.com/vitejs/vite/issues/2369#issuecomment-904689947

**环境变量注入**

使用 `define` 配置实现原 Webpack DefinePlugin 相同的效果。

**路径别名**

将原 `alias: { name: path }` 配置格式改为 `alias: [{ find: name, replacement: path }]` 格式。

**在 scss 中引入 node_modules 目录中的样式**

在 `alias` 配置中添加以下替换规则以支持用 `@import "~xxxxx"` 将路径定向到 `node_modules` 目录内。

```s
{
  find: /~(.+)/,
  replacement: path.join(__dirname, 'node_modules/$1'),
}
```

**文件复制**

暂未找到合适的文件复制插件，所以手写 JavaScript 代码在每次加载 `vite.config.js` 时复制一次文件。

**@import must precede all other statements (besides @charset)**

在 `.styl` 文件中使用 `@import` 引入 css 文件会报这个警告，编译结果是每个 css 都被输出成单个文件，解决方法是新建个 vendor.scss 然后将 css 文件都集中在里面 `@import`。

## 效果

**启动速度很快。** vite 将打包程序的部分工作交给了浏览器，仅在浏览器请求源码时进行转换并按需提供源码，这使得开发服务器在启动时的工作量非常少，不用像 webpack 那样要耗费大量时间等待打包完成才能使用开发服务器。

**性能和内存开销降低。** 初次访问开发服务器资源时的性能和内存开销比较大，node 和 esbuild 进程共占用约 1.4 GB 内存，但在构建完后会降低至 500MB 以下。

**热更新很快。** 修改代码后，相关组件会迅速更新为修改后的效果，不用再像之前那样浪费时间等待页面刷新了。

## 总结

vite 解决了开发环境下的构建性能开销大和耗时长的问题，使得项目成员有更多的时间用于开发上，提升了开发效率。目前暂未替换 webpack，仅作为另一个可选的构建方式。
