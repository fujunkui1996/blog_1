---
title: Webpack DLLPlugin 问题处理记录
date: 2020-05-14 10:36:30
tags:
    - webpack
    - 插件
---

最近在给企业版页面引入 gitee-vendor-dll 包中的 echarts 库后出了点问题，部分未使用 echarts 的页面会报错未找到 echarts 依赖库，初步推测问题原因出在 DllPlugin 和 DllReferencePlugin 上。

{% asset_img echarts-not-defined.png '未找到 echarts' %}

webpack 文档上对 DllReferencePlugin 插件的[工作模式介绍](https://webpack.docschina.org/plugins/dll-plugin/#%E6%98%A0%E5%B0%84%E6%A8%A1%E5%BC%8F-mapped-mode-)是这样的：

> dll 中的内容被映射到了当前目录下。如果一个被 require 的文件符合 dll 中的某个文件(解析之后)，那么这个dll中的这个文件就会被使用。

在使用 DllReferencePlugin 插件时，自然会习惯性认为它只会给主动引入了 echarts 的模块添加 echarts 依赖，然而从实际效果来看，它会给 entry 配置中的所有入口文件加上 echarts 依赖，假设 DllReferencePlugin 确实能够正常工作，那么问题原因可能在于输出的 dll 包含了除 echarts 外全局共用的模块。

先看看 webpack 为 echarts 输出的 manifest.json 内容：

```json
{
	"name": "echarts_lib",
	"content": {
		"../node_modules/echarts/dist/echarts.common.js": {
			"id": "./node_modules/echarts/dist/echarts.common.js",
			"buildMeta": {
				"providedExports": true
			}
		},
		"../node_modules/webpack/buildin/global.js": {
			"id": "./node_modules/webpack/buildin/global.js",
			"buildMeta": {
				"providedExports": true
			}
		}
	}
}
```

内容很简单，可以看到多了个 `webpack/buildin/global.js` 文件，经测试，删掉这项记录可以解决问题，那么该如何让 manifest.json 不包含这个文件？有两个选择：

1. 向 webpack 反馈问题，让官方给出标准解决方案
1. 手写 JavaScript 代码，在 webpack 执行完后更新 manifest.json 文件
