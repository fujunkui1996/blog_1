---
title: Gitee 企业版 Pull Request 详情页开发总结
date: 2020-10-11 19:00:00
tags: react
---
在 Gitee 企业版的 React 重构计划中，Pull Request 详情页开发算是一个有点复杂度的任务，主要涉及到组件化、数据模型设计、组件通信等技术点，其开发过程中的设计和思考比较适合总结输出成文章，而在以后的维护和改进中，有文章参考比直接看代码更容易了解到现有的设计，也更容易做出更合适的改动。

## 需求分析

Pull Request 详情页分为上下两个部分：

- 上半部分区域包含标题、描述、状态等信息以及一些操作按钮，和其它页面的功能差不多，涉及到更新的操作都有权限判断，操作后会触发更新操作日志。
- 下半部分区域由评论、提交记录和文件改动三个选项卡组成，其中评论和文件改动共享同一个评论数据源，文件改动中的代码评论会呈现在评论中，在评论中点击代码评论的“查看详情”链接则会切换到文件改动选项卡并滚动到对应评论。

## 技术选型

- **文件差异对比组件：** 基于 [react-diff-view](https://www.npmjs.com/package/react-diff-view) 实现。只需要调整样式、添加评论功能和视图模式切换按钮即可。原本企业版的文件差异接口会解析一遍 diff 文本然后把结果返回给前端用，但现在不需要了，因为 react-diff-view 能直接解析 diff 文本并渲染，所以还需要让后端调整接口返回原始的 diff 文本。
- **状态管理：** 用 React 现有的功能实现。现在的复杂度还没达到要用 Redux 的程度，毕竟曾经有人说过这样一句话：

    > 如果你不知道是否需要 Redux，那就是不需要它。

## 组件化

根据视觉区域、代码量、职责、耦合度等考量因素将页面划分成多个组件，组件及结构如下：

- PullRequestDetail
  - PullRequestDetailMeta
  - PullRequestDetailForm
  - PullRequestDetailToolbar
  - PullRequestDetailActions
  - PullRequestDetailLabels
  - PullRequestDetailReactions
  - PullRequestDetailIssues
  - PullRequestDetailTabs
    - PullRequestDetailComments
    - PullRequestDetailCommits
    - PullRequestDetailFiles

子组件名称以 PullRequestDetail 开头是为了标识它们是 PullRequestDetail 的子组件，与通用组件区分。

## 数据模型

由需求分析结果可知有两个数据源：

- **Pull Request 详情：** 包含 Pull Request 的状态、标题、描述、审查测试人员等基本信息以及当前用户对它的权限，这些数据在多个组件中都需要用到。
- **评论列表：** 评论和文件改动都需要读写评论数据。

以现有的组件数量和复杂度，如果用按需将状态和函数以 props 传递的这种常规方式来实现的话会让代码变得更复杂，组件的依赖关系和数据流向如下图所示：

{% asset_img 'pull-request-component-relationship-before.png' '组件依赖关系' %}

PullRequestDetail 作为根组件还夹杂了一堆状态管理代码，考虑到模块化思想中的单一职责和低耦合原则，应该将这些组件依赖的状态和函数汇聚成独立的状态模块（PullReqeustStore）来使用，这样就能简化 PullRequestDetail 的代码，使其只需专注于表达页面结构，就像书的目录专注于索引各个章节的标题和页码。

{% asset_img 'pull-request-component-relationship-after.png' '组件依赖关系' %}

评论数据的操作方式比较多且逻辑复杂，包含加载、删除、更新、回复和追加，因此采用 useReducer() 来创建 state 以及与其配套的 dispatch 方法。评论的 state 和 dispatch 方法的共享采用 Context 实现，由 PullRequestDetailTabs 组件分发给 PullRequestDetailComments 和 PullRequestDetailCommits 使用，这些都是 React 的基本用法，本文就不再详细说明了。
