---
title: 看板组件的分离工作记录
date: 2021-06-02 16:58:48
tags:
  - react
  - 看板
  - 工作记录
---
## 背景

现有的看板组件与业务代码存在耦合，而且由于初期考虑不够全面，还存在一些设计不合理的地方，因此应该对其进行改进并分离到业务组件库中。

## 原计划

- 将看板组件分离到独立的代码库中维护，解除与业务代码的耦合。
- 重写看板组件的代码，遵循推荐的 eslint 规则，提升代码质量。
- 采用 react-beautiful-dnd 实现拖拽操作，提升交互体验。
- 采用 react-window 实现虚拟滚动加载任务卡片，提升交互体验和性能。

## 遇到的问题

在完成看板组件的基本功能和一个简单的示例后，出现了以下问题：

- **性能变差。** 当板块和卡片数量变多后，拖拽卡片会卡顿现象，滚动触发加载下一页任务列表时也有卡顿现象。
- **数据通信成本变高。** 如果像官方示例那样，所有板块都共用父组件传入的数据源，那么在拖拽卡片时会很容易触发重新渲染，导致性能变差。
- **布局实现难度变高。** react-window 要求容器高度固定，这导致看板不能直接利用 css 布局特性实现自动拉伸高度。

## 实际改动内容

考虑到现阶段不适合投入太多时间成本去解决这些问题，所以我们只对现有代码做简单的重构和优化，不做大改动，实际改动内容大致如下：

- 将看板组件代码移动到业务组件库的代码库中。
- 改进看板现有的基于事件的数据通信方式。
- 使用 prop-types 添加属性类型检查。
- 改进滚动加载功能。
- 升级 react-dnd。

## 实现细节

### 滚动加载

在窗口尺寸变化、页面内任意元素滚动的时候，检测卡片列表是否在可见区域内、是否已经滚动到接近底部，然后决定是否加载下一页数据，这两个检测的核心代码如下：

```js
const viewPortWidth = Math.min(document.body.offsetWidth, parent.offsetWidth);
const visible = el.offsetLeft < parent.offsetLeft + parent.scrollLeft + viewPortWidth;
const loadable = el.scrollTop + el.clientHeight >= el.scrollHeight - el.clientHeight / 5;
```

### 数据通信

每个板块维护自己的卡片列表数据，利用事件实现跨板块的数据操作，能减少状态变化的影响范围和重新渲染次数。

事件监听和触发由 EventHub 对象实现，Board 组件和 Column 组件共享这个对象，我们可以使用 `<KanbanEventHubProvider>` 和 `useKanbanEventHub()` 拿到它，示例如下：

```jsx
function KanbanIssueEventHandler({ children }) {
  const kanbanEventHub = useKanbanEventHub();

  // ...
    // 在合适的时机向 example 板块插入新项目
    kanbanEventHub.emit('insertItem', { columnId: 'example',  item });
  // ...

  return children;
}

function KanbanBoardWrapper({ children }) {
  return (
    <KanbanEventHubProvider>
      <KanbanIssueEventHandler>
        {children}
      </KanbanIssueEventHandler>
    </KanbanEventHubProvider>
  );
}

function Example() {
  // ...
  return (
    <KanbanBoardWrapper>
      <kanbanBoard
        columns={...}
        renderCard={...}
        onSort={...}
      />
    </KanbanBoardWrapper>
  );
}
```

## 效果演示

http://gitee-frontend.gitee.io/gitee-ent-react/gitee-kanban-react/
